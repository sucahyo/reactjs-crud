import React, { Component } from 'react';
import DataTable from 'react-data-table-component';
import axios from 'axios';
// import { storiesOf } from '@storybook/react';
import $ from 'jquery';

const columns = [
  {
    name: 'Title',
    sortable: true,
    selector: 'first_name',
    cell: row => <div><div style={{ fontWeight: 'bold' }}>{row.first_name}</div>{row.email}</div>,
  },
  {
    name: 'Year',
    selector: 'year',
    sortable: true,
  },
  {
    name: 'Opsi',
    cell: row => <div>Click</div>,
  },
];

export default class DatatablesServerSide extends Component {
  state = {
    data: [],
    loading: false,
    totalRows: 0,
    perPage: 10,
  };
  async componentDidMount() {
    const { perPage } = this.state;
    this.setState({ loading: true });
    const response = await axios.get(`https://reqres.in/api/users?page=1&per_page=${perPage}`);

    this.setState({
      data: response.data.data,
      totalRows: response.data.total,
      loading: false,
    });

    setTimeout(
      $('.gVZomo').css('overflow-x', 'none')
      , 3000);

  }

  handlePageChange = async page => {
    const { perPage } = this.state;

    this.setState({ loading: true });

    const response = await axios.get(
      `https://reqres.in/api/users?page=${page}&per_page=${perPage}`
    );

    this.setState({
      loading: false,
      data: response.data.data,
    });
  };

  handlePerRowsChange = async (perPage, page) => {
    this.setState({ loading: true });

    const response = await axios.get(
      `https://reqres.in/api/users?page=${page}&per_page=${perPage}`
    );

    this.setState({
      loading: false,
      data: response.data.data,
      perPage,
    });
  };

  render() {
    console.log('data - ', this.state.data);
    return (
      <DataTable
        title='Cahyo Table'
        data={this.state.data}
        pagination
        columns={columns}
        onRowSelected={this.handleChange}
        selectableRows
        progressPending={this.state.loading}
        paginationServer
        paginationTotalRows={this.state.totalRows}
        onChangeRowsPerPage={this.handlePerRowsChange}
        onChangePage={this.handlePageChange}
      />
    )
  }
}
