import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
} from "react-router-dom";
import '../src/App.css';

import Create from './components/create.component';
import Edit from './components/edit.component';
import Index from './components/index.component';
import DatatablesServerSide from './components/DatatablesServerSide';

function App() {
  return (
    <Router>
      <div className="container">
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <Link to={'/'} className="navbar-brand">React CRUD Example</Link>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item">
                <Link to={'/'} className="nav-link">Home</Link>
              </li>
              <li className="nav-item">
                <Link to={'/create'} className="nav-link">Create</Link>
              </li>
              <li className="nav-item">
                <Link to={'/index'} className="nav-link">Index</Link>
              </li>
              <li className="nav-item">
                <Link to={'/datatablesserverside'} className="nav-link">Datatables Server Side</Link>
              </li>
            </ul>
          </div>
        </nav> <br />
        <h2>Welcome to React CRUD Tutorial</h2> <br />
        <Switch>
          <Route exact path='/' component={Home} />
          <Route exact path='/create' component={Create} />
          <Route path='/edit/:id' component={Edit} />
          <Route path='/index' component={Index} />
          <Route exact path='/datatablesserverside' component={DatatablesServerSide} />
        </Switch>
      </div>
    </Router>
  );
}

function Home() {
  return (
    <p>
      A <code>&lt;Switch></code> renders the first child <code>&lt;Route></code>{" "}
      that matches. A <code>&lt;Route></code> with no <code>path</code> always
      matches.
    </p>
  );
}

export default App;
